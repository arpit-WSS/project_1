import React from 'react';
import AppBarComponent  from './components/AppBarComponent/AppBarComponent';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HomePage from './components/HomePage/HomePage';
import { SearchCountry } from './components/SearchCountry/SearchCountry';
import { Information } from './components/Information/Information';


function App() {
	return (
		<BrowserRouter>
			
			<AppBarComponent/>
				<Routes>
					<Route path="/" element={<HomePage/>} />
					<Route path="/:countryName" element={<Information/>}/>
				</Routes>
			
		</BrowserRouter>
		
	);
}

export default App;
