import React from 'react';

import { AppBar, Toolbar, Typography, Box } from '@mui/material';

const AppBarComponent = (): JSX.Element => {
	return (
		<Box>
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="h5" align="center" sx={{ width: '100%' }}>
						World Weather Information
					</Typography>
				</Toolbar>
			</AppBar>
		</Box>
	);
};

export default AppBarComponent;
