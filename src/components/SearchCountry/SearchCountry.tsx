import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography'
import worldmap from '../../Images/worldmap.jpg';
import { useNavigate } from 'react-router-dom';

export const SearchCountry = ()=> {
	const [ countryName, setcountryName ] = useState('');
	const [ error, setError ] = useState('');
	const navigate = useNavigate();

	const handleChange = (e : React.ChangeEvent<HTMLInputElement>) => {
		setcountryName(e.target.value);
	};

	const handleSubmit = (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		try {
			setError('');
			navigate(`./${countryName}`);
		} catch (error) {
			setError('No such country');
		}
	};

	return (
		<Box sx={{ flexGrow: 1 }}>
			<Grid container xs={12} sx={{ minHeight: '720px' }} justifyContent="center" alignItems="center">
				<Grid container item md={6} xs={12} justifyContent="center" alignItems="center">
					<img src={worldmap} />
				</Grid>

				<Grid
					container
					item
					md={6}
					xs={12}
					justifyContent="center"
					alignItems="center"
					direction="column"
					sx={{ height: 450 }}
				>
					<Grid item>
						<TextField
							variant="outlined"
							label="Enter Country"
							margin="normal"
							size="small"
							onChange={handleChange}
						/>
					</Grid>
					<Grid item>
						<Button variant="contained" disabled={countryName.length < 1} onClick={(e) => handleSubmit(e)}>
							Submit
						</Button>
					</Grid>
				</Grid>
			</Grid>
		</Box>
	);
};
