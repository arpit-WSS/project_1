import { render, screen } from "@testing-library/react";
import { SearchCountry } from "./../SearchCountry"

beforeEach(()=>{
    
})

it("check if button is disabled", ()=>{
    render(<SearchCountry/>);
    const element = screen.getByText('/Submit/i').closest('button')
    expect(element).toBeDisabled()
})