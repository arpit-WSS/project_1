import { Card, CardActions, CardContent, Button, Grid, Typography, Divider, CardMedia } from '@mui/material';
import { useParams } from 'react-router-dom';
import { useFetchCountryData } from '../../Hooks/useFetchCountryData';
import { useFetchCapitalData } from '../../Hooks/useFetchCapitalData';
import { useState } from 'react';

export const Information = () => {
	const [ getCapitalName, setGetCapitalName ] = useState('');
	const [ weatherInfo, setWeatherInfo ] = useState(false);
	const { countryName } = useParams();
	const { data, loading, error } = useFetchCountryData(countryName);
	const { weatherData, loading2, error2 } = useFetchCapitalData('New Delhi');

	const handleclick = (e) => {
		console.log(weatherData);
		// data.name.map((item) => {
		// 	// setGetCapitalName(item['capital'][0]);
		// });
		// setWeatherInfo(true);
	};

	return (
		<Grid container xs={12} sx={{ minHeight: '720px' }} justifyContent="center" alignItems="center">
			<Grid container item>
				<Card variant="outlined" sx={{ minWidth: '450px', backgroundColor: '#d8e2dc' }}>
					{!loading &&
						data.name.map((cdata) => (
							<CardContent>
								<Typography variant="h5">Country: {countryName}</Typography>
								<Divider />
								<Typography my={2}>Capital: {cdata['capital'][0]}</Typography>
								<CardMedia
									component="img"
									image={cdata['flags']['png']}
									height="200"
									alt="country flag"
								/>
								<Typography my={2}>Population: {cdata['population']}</Typography>
								<Typography mt={2}>Latitude: {cdata['latlng'][0]}</Typography>
								<Typography mt={2}>Longitude: {cdata['latlng'][1]}</Typography>
							</CardContent>
						))}

					<CardActions>
						{!loading && (
							<Button
								align="center"
								variant="contained"
								onClick={(e) => {
									handleclick(e);
								}}
							>
								Capital Weather
							</Button>
						)}
					</CardActions>
				</Card>
			</Grid>
			<Grid item>{weatherInfo && 'Hello World'}</Grid>
		</Grid>
	);
};
