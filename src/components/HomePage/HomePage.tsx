import React from 'react';
import { SearchCountry } from '../SearchCountry/SearchCountry';

const HomePage = () => {
	return (
		<React.Fragment>
			<SearchCountry />
		</React.Fragment>
	);
};

export default HomePage;
