import React from 'react';
import { useState, useEffect } from 'react';
import { APIs } from '../API';

const initialState = {
	wData: []
};

export const useFetchCapitalData = (capitalName) => {
	const [ weatherData, setWeatherData ] = useState(initialState);
	const [ loading2, setLoading ] = useState(false);
	const [ error2, setError ] = useState(false);

	const fetchData = async (capitalName) => {
		try {
			setError(false);
			setLoading(true);
			const data = await APIs.fetchWeatherInformation(capitalName);
			setWeatherData({ wData: [ ...data ] });
		} catch (error) {
			setError(true);
		}
		setLoading(false);
	};

	useEffect(() => {
		setWeatherData(initialState);
		fetchData(capitalName);
	}, []);

	return { weatherData, loading2, error2 };
};
