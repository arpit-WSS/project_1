import React from 'react';
import { useState, useEffect } from 'react';
import { APIs } from '../API';
import { useParams } from 'react-router-dom';

const initialState = {
	name: []
};

export const useFetchCountryData = () => {
	const [ data, setdata ] = useState(initialState);
	const [ loading, setLoading ] = useState(false);
	const [ error, setError ] = useState(false);
	const { countryName } = useParams();

	const fetchData = async (countryName) => {
		try {
			setError(false);
			setLoading(true);
			const countrydata = await APIs.fetchCountryInformation(countryName);
			setdata({ name: [ ...countrydata ] });
		} catch (error) {
			setError(true);
		}
		setLoading(false);
	};

	useEffect(() => {
		setdata(initialState);
		fetchData(countryName);
	}, []);

	return { data, loading, error };
};
