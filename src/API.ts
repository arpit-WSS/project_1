import { API_KEY } from './config';


export const APIs = {
	fetchCountryInformation: async (countryName: string) => {
		const countryInformation = `https://restcountries.com/v3.1/name/${countryName}`;

		return await (await fetch(countryInformation)).json();
	},

	fetchWeatherInformation: async (CapitalCityName: string) => {
		const weatherEndPoint = `https://api.weatherstack.com/current?access_key=${API_KEY}&query=${CapitalCityName}`;

		return await (await fetch(weatherEndPoint)).json();
	}
};
